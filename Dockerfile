FROM registry.gitlab.com/fdroid/ci-images-client:latest

RUN apt update -y                           \
 && apt install -y build-essential xz-utils \
 && rm -rf /var/lib/apt/lists/*

RUN sdkmanager             \
    "platforms;android-28" \
    "ndk-bundle"           \
    "build-tools;28.0.3"   \
    "platform-tools"       \
    "tools"                \
    >/dev/null

ENV JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
ENV PATH=$PATH:$JAVA_HOME/bin

ARG android_arch=x86
ENV ANDROID_ARCH=$android_arch
ARG qt_version=5.13.1
ARG qt_short_version=5.13
ARG qt_url=http://download.qt.io/archive/qt/$qt_short_version/$qt_version/submodules/

ARG qt_qtbase_pkg=qtbase-everywhere-src-$qt_version
ARG qt_qtbase_url=$qt_url/$qt_qtbase_pkg.tar.xz

ENV QT=/qt

RUN curl -L $qt_qtbase_url | tar -x --xz      \
 && cd $qt_qtbase_pkg                         \
 && ./configure                               \
        -xplatform android-clang              \
        --disable-rpath                       \
        -nomake tests                         \
        -nomake examples                      \
        -no-warnings-are-errors               \
        -android-arch $ANDROID_ARCH           \
        -android-sdk $ANDROID_HOME            \
        -android-ndk $ANDROID_HOME/ndk-bundle \
        --prefix=$QT                          \
        -opensource                           \
        -confirm-license                      \
 && make -j$(nproc)                           \
 && make install                              \
 && cd ..                                     \
 && rm -rf $qt_qtbase_pkg

ARG qt_androidextras_pkg=qtandroidextras-everywhere-src-$qt_version
ARG qt_androidextras_url=$qt_url/$qt_androidextras_pkg.tar.xz

ENV PATH="$QT/bin:$PATH"

RUN curl -L $qt_androidextras_url | tar -x --xz \
 && cd $qt_androidextras_pkg                    \
 && qmake .                                     \
 && make -j$(nproc)                             \
 && make install                                \
 && cd ..                                       \
 && rm -rf $qt_androidextras_pkg
